﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Security.Principal;

class Matrix
{
    public List<List<double>> ryadky;
    List<double> singuliarniChisla = new List<double>();
    List<List<double>> liviSinguliarniVectori;
    List<List<double>> praviSinguliarniVectori;
    double epsilon = 0.01;
    int rang = 0;
    double chisloObumovlenosti1 = 0;
    double chisloObumovlenosti2 = 0;
    List<List<double>> psevdoObernena;
    public double normaNeskinchennist = 0;
    int m;
    int n;
    public List<double> x0 = new List<double>();
    public List<double> x = new List<double>();
    public List<double> b;
    bool ready = false;
    List<Matrix> children = new List<Matrix>();
    public Matrix(int n, int m)
    {
        ryadky = new List<List<double>>();
        for (int i = 0; i < n; i++)
        {
            ryadky.Add(new List<double>());
            for (int j = 0; j < m; j++)

            {
                ryadky[i].Add(0);
            }
        }
        liviSinguliarniVectori = OdinichnaMatricia(n);
        praviSinguliarniVectori = OdinichnaMatricia(m);
        this.m = n;
        this.n = m;
        
    }
    bool Children()
    {
        foreach(Matrix m in children)
        {
            if(m.ready == false)
            {
                return false;
            }
        }
        foreach (Matrix m in children)
        {
            foreach (double d in m.singuliarniChisla)
            {
                singuliarniChisla.Add(d);
            }
        }
        return true;
    }
    bool check()
    {
        for (int i = 1; i < ryadky.Count; i++)
        {
            if(Math.Abs(ryadky[i][i - 1]) < epsilon)
            {
                singuliarniChisla.Add(ryadky[i][i]);
                Console.WriteLine("!!!");
                Matrix m = new Matrix(ryadky.Count - i, ryadky.Count - i);
                m.ryadky = copyMatrix(ryadky, i + 1);
                Matrix m2 = new Matrix(ryadky.Count - i - 1, ryadky.Count - i - 1);
                m.ryadky = copyMatrix(i - 1, ryadky);
                m.DrugiyEtap();
                m2.DrugiyEtap();
                children.Add(m);
                children.Add(m2);
                m.OtrimatiSinguliarniChisla(m.ryadky);
                m2.OtrimatiSinguliarniChisla(m2.ryadky);
                return true;
            }
        }
        for (int i = 0; i < ryadky.Count; i++)
        {
            
                if (Math.Abs(ryadky[i][i]) < epsilon)
                {
                Console.WriteLine("!!!");
            }
            return false;
            }
        return false;
        
        
    }
    List<List<double>> copyMatrix(List<List<double>> matrix, int index)
    {
        List<List<double>> res = new List<List<double>>();
        for (int i = n; i < matrix.Count; i++)
        {
            res.Add(new List<double>());
            for (int j = n; j < matrix.Count; j++)
            {
                res[i - n].Add(matrix[i][j]);
            }
        }
        return res;
            }
    List<List<double>> copyMatrix(int index, List<List<double>> matrix)
    {
        List<List<double>> res = new List<List<double>>();
        for (int i = 0; i <= index; i++)
        {
            res.Add(new List<double>());
            for (int j = 0; j <= index; j++)
            {
                res[i].Add(matrix[i][j]);
            }
        }
        return res;
    }
    void PsevdoRozviazok()
    {
        if (m > n || n > m)
        {
            for (int i = 0; i < psevdoObernena.Count; i++)
            {
                x0.Add(0);
                for (int j = 0; j < psevdoObernena[0].Count; j++)
                {
                    x0[i] += psevdoObernena[i][j] * b[j];
                }
            }
            for(int i = x0.Count; i < m; i++)
            {
                x0.Add(0);
            }
        }
        else
        {

        }
    }
    void NormalniyRozviazok()
    {
        List<double> Z = MnojenniaMatriciNaVector(Transponuvati(liviSinguliarniVectori), b);
        List<double> y = new List<double>();
        if (Z.Count > rang)
        {
            for(int i = rang - 1; i < Z.Count; i++)
            {
                if(Z[i] > 0)
                {
                    Console.WriteLine("Система не має класичного розв'язку");
                    return;
                }
            }
        }
        for(int i = 0; i < Math.Min(Z.Count, singuliarniChisla.Count); i++)
        {
            y.Add(Z[i] / singuliarniChisla[i]);
        }
        x = MnojenniaMatriciNaVector(Transponuvati(praviSinguliarniVectori), y);
        for (int i = x.Count; i < m; i++)
        {
            x.Add(0);
        }
    }
    List<double> MnojenniaMatriciNaVector(List<List<double>> matrix, List<double> vector) {
        List<double> dobutok = new List<double>();
        for (int i = 0; i < matrix[0].Count; i++)
        {
            dobutok.Add(0);
            for (int j = 0; j < Math.Min(matrix[0].Count, vector.Count); j++)
        {
                dobutok[i] += matrix[i][j] * vector[j];
        }
        }
        return dobutok;

    }
    void ObchislitiRang()
    {
        for(int i = 0; i < singuliarniChisla.Count; i++)
        {
            if(singuliarniChisla[i] >= epsilon)
            {
                rang++;
            }
        }
        
    }
    public static List<List<double>> Transponuvati(List<List<double>> matrix)
    {
        List<List<double>> resultat = new List<List<double>>();
        for (int i = 0; i < matrix[0].Count; i++)
        {
            resultat.Add(new List<double>());
            for (int j = 0; j < matrix.Count; j++)
            {
                resultat[i].Add(matrix[j][i]);
            }
        }
        return resultat;
    }
    public static void showMatrix(List<List<double>> matrix)
    {
        for (int i = 0; i < matrix.Count; i++)
        {
            for (int j = 0; j < matrix[0].Count; j++)

            {
                for (int k = 0; k < 20 - Convert.ToString((float)matrix[i][j]).Length; k++)
                {
                    Console.Write(" ");
                }
                Console.Write((float)matrix[i][j]);
            }
            Console.WriteLine();
        }
    }
    List<double> Stovpchik(List<List<double>> matrix, int index)
    {
        List<double> res = new List<double>();
        for(int i = 0; i < matrix[0].Count; i++)
        {
            res.Add(matrix[i][index]);
        }
        return res;
    }
    void DokinutiRiadok(List<List<double>> matrix, bool odinicia)
    {
        matrix.Add(new List<double>());
        for(int j = 0; j < matrix[0].Count; j++)
        {
            if(j == matrix.Count - 1 && odinicia)
            {
                matrix[matrix.Count - 1].Add(1);
                continue;
            }
            matrix[matrix.Count - 1].Add(0);
        }
    }
    void DokinutiStovpec(List<List<double>> matrix, bool odinicia)
    {
        
        for (int j = 0; j < matrix.Count; j++)
        {
            if (j == matrix[0].Count - 1 && odinicia)
            {
                matrix[j].Add(1);
                continue;
            }
            matrix[j].Add(0);
        }
    }
    void OtrimatiSinguliarniChisla(List<List<double>> matrix)
    {
        for(int i = 0; i < matrix.Count; i++)
        {
            if(matrix[i][i] < 0)
            {
                matrix[i][i] = -matrix[i][i];
                InvertuvatiRiadok(praviSinguliarniVectori, i);
            }
            singuliarniChisla.Add(matrix[i][i]);
        }
    }
    void InvertuvatiRiadok(List<List<double>> matrix, int index)
    {
        for (int j = 0; j < matrix[0].Count; j++){
            matrix[index][j] = -matrix[index][j];
        }

    }
    public void SinguliarniyRozklad()
    {
        Stopwatch sw = new Stopwatch();
        sw.Start();
        PershiyEtap();
        DrugiyEtap();
        sw.Stop();
        Console.WriteLine("Час : " + sw.Elapsed);
        OtrimatiSinguliarniChisla(ryadky);
        PochatkovaRozmirnist();
        ObchislitiRang();
        PsevdoObernena();
        ObchislitiChisloObumovlenosti();
        PsevdoRozviazok();
        NormalniyRozviazok();
        showRes();
    }
    void showRes()
    {
        Console.WriteLine("Матриця Сигма");
        showMatrix(ryadky);
        Console.WriteLine();
        Console.WriteLine("Ліві сингулярні вектори");
        for(int j = 0; j < liviSinguliarniVectori[0].Count; j++)
        {
            Console.WriteLine("Вектор " + j);
            showVector(Stovpchik(liviSinguliarniVectori, j));
        }
        //showMatrix(liviSinguliarniVectori);
        Console.WriteLine();
        Console.WriteLine("Праві сингулярні вектори");
        for (int j = 0; j < praviSinguliarniVectori[0].Count; j++)
        {
            Console.WriteLine("Вектор " + j);
            showVector(Stovpchik(praviSinguliarniVectori, j));
        }
        //showMatrix(praviSinguliarniVectori);
        Console.WriteLine();
        Console.WriteLine("Число обумовленості за сингулярними числами: " + chisloObumovlenosti1);
        Console.WriteLine();
        Console.WriteLine("Число обумовленості за псевдооберненою матрицею: " + chisloObumovlenosti2);
        Console.WriteLine();
        Console.WriteLine("Ранг: " + rang);
        Console.WriteLine("Псевдообернена матриця: ");
        showMatrix(psevdoObernena);
        Console.WriteLine("Псевдорозв'язок СЛАР: ");
        showVector(x0);
        Console.WriteLine("Нормальний розв'язок СЛАР: ");
        showVector(x);
    }
    void ObchislitiChisloObumovlenosti()
    {
        double naibilshe = singuliarniChisla[0];
        for (int i = 1; i < singuliarniChisla.Count; i++)
        {
            if (singuliarniChisla[i] > naibilshe)
            {
                naibilshe = singuliarniChisla[i];
            }
        }
        double naimenshe = singuliarniChisla[0];
        for (int i = 1; i < singuliarniChisla.Count; i++)
        {
            if (singuliarniChisla[i] < naimenshe)
            {
                naimenshe = singuliarniChisla[i];
            }
        }
        chisloObumovlenosti1 = naibilshe / naimenshe;
        chisloObumovlenosti2 = normaNeskinchennist * NormaNeskinchennist(psevdoObernena);
    }
    void PsevdoObernena()
    {
        psevdoObernena = new List<List<double>>();
        for (int i = 0; i < m; i++)
        {
            psevdoObernena.Add(new List<double>());
            for (int j = 0; j < n; j++)
            {
                if (i != j)
                {
                    psevdoObernena[i].Add(0);
                }
                else
                {
                    psevdoObernena[i].Add(1 / ryadky[j][j]);
                }
            }
        }
        psevdoObernena = Transponuvati(psevdoObernena);
        psevdoObernena = MnojenniaMatriciNaMatriciu(Transponuvati(praviSinguliarniVectori), psevdoObernena);
        psevdoObernena = MnojenniaMatriciNaMatriciu(psevdoObernena, Transponuvati(liviSinguliarniVectori));
    }
    public double NormaNeskinchennist(List<List<double>> matrix)
    {
        double norma = 0;
        for(int i = 0; i < matrix.Count; i++){
            double sum = 0;
            for(int j = 0; j < matrix[0].Count; j++)
            {
                sum += Math.Abs(matrix[i][j]);
            }
            if(sum > norma)
            {
                norma = sum;
            }
        }
        return norma;
    }
    void PochatkovaRozmirnist()
    {
        for(int i = ryadky.Count; i < m; i++)
        {
            DokinutiRiadok(ryadky, false);
        }
        for(int i = ryadky[0].Count; i < n;  i++)
        {
            DokinutiStovpec(ryadky, false);
        }
    }
    public static List<List<double>> MnojenniaMatriciNaMatriciu(List<List<double>> mnojene, List<List<double>> mnojnik)
    {
        List<List<double>> dobutok = new List<List<double>>();
        if (mnojene[0].Count != mnojnik.Count)
        {
            Console.WriteLine("Не узгоджені");
            return null;
        }
        for (int i = 0; i < mnojene.Count; i++)
        {
            dobutok.Add(new List<double>());
            for (int j = 0; j < mnojnik[0].Count; j++)
            {
                double sum = 0;
                for (int k = 0; k < mnojene[0].Count; k++)
                {
                    sum += mnojene[i][k] * mnojnik[k][j];
                }
                dobutok[i].Add(sum);
            }
        }
        return dobutok;
    }
    List<List<double>> OdinichnaMatricia(int n)
    {
        List<List<double>> odinichna = new List<List<double>>();
        for (int i = 0; i < n; i++)
        {
            odinichna.Add(new List<double>());
            for (int j = 0; j < n; j++)
            {
                if (i != j)
                {
                    odinichna[i].Add(0);
                }
                else
                {
                    odinichna[i].Add(1);
                }
            }
        }
        return odinichna;
    }
    List<List<double>> MnojenniaVectoraNaVector(List<double> mnojene, List<double> mnojnik)
    {
        List<List<double>> dobutok = new List<List<double>>();
        for (int i = 0; i < mnojnik.Count; i++)
        {
            dobutok.Add(new List<double>());
            for (int j = 0; j < mnojnik.Count; j++)
            {
                dobutok[i].Add(mnojene[i] * mnojnik[j]);
            }
        }
        return dobutok;
    }
    List<List<double>> MnojenniaMatriciNaScaliar(double mnojene, List<List<double>> mnojnik)
    {
        List<List<double>> dobutok = new List<List<double>>();
        for (int i = 0; i < mnojnik.Count; i++)
        {
            dobutok.Add(new List<double>());
            for (int j = 0; j < mnojnik[0].Count; j++)
            {
                dobutok[i].Add(mnojene * mnojnik[i][j]);
            }
        }
        return dobutok;
    }
    List<List<double>> VidkinutiRyadok(List<List<double>> matrix, int index)
    {
        List<List<double>> res = new List<List<double>>();
        
        for (int i = 0; i < matrix.Count; i++)
        {
            if (i != index)
            {
                res.Add(new List<double>());
                for (int j = 0; j < matrix[0].Count; j++)
                {
                    res[res.Count - 1].Add(matrix[i][j]);
                }
            }
        }
        return res;
    }
    List<List<double>> VidkinutiStovpec(List<List<double>> matrix, int index)
    {
        List<List<double>> res = new List<List<double>>();

        for (int i = 0; i < matrix.Count; i++)
        {
            
                res.Add(new List<double>());
                for (int j = 0; j < matrix[0].Count; j++)
                {
                if (j != index)
                {
                    res[res.Count - 1].Add(matrix[i][j]);
                }
            }
        }
        return res;
    }
    List<List<double>> VidnimanniaVidMatriciMatrici(List<List<double>> zmenshuvane, List<List<double>> vidiemnik)
    {
        List<List<double>> riznitsia = new List<List<double>>();
        for (int i = 0; i < zmenshuvane.Count; i++)
        {
            riznitsia.Add(new List<double>());
            for (int j = 0; j < vidiemnik.Count; j++)
            {
                riznitsia[i].Add(zmenshuvane[i][j] - vidiemnik[i][j]);
            }
        }
        return riznitsia;
    }
    void showVector(List<double> vector)
    {
        foreach(double d in vector)
        {
            Console.WriteLine(d);
        }
    }
    public void PershiyEtap()
    {
        
        if (m > n)
        {
            for (int i = 0; i < n; i++)
            {
                //if (i != n - 1)
                {
                    List<List<double>> H2 = new List<List<double>>();
                    double p = 0;
                    for (int j = i; j < n; j++)
                    {
                        p += ryadky[i][j] * ryadky[i][j];
                    }

                    p = Math.Sign(-ryadky[i][i]) * Math.Sqrt(p);
                    double u = 1 / (Math.Sqrt(2 * p * p - 2 * p * ryadky[i][i]));
                    List<double> w = new List<double>();
                    for (int j = 0; j < ryadky[i].Count; j++)
                    {
                        if (j < i)
                        {
                            w.Add(0);
                        }
                        else if (j == i)
                        {
                            w.Add(u * (ryadky[j][j] - p));
                        }
                        else
                        {
                            w.Add(u * ryadky[i][j]);
                        }
                    }


                    H2 = VidnimanniaVidMatriciMatrici(OdinichnaMatricia(ryadky[0].Count), MnojenniaMatriciNaScaliar(2, MnojenniaVectoraNaVector(w, w)));
                    ryadky = MnojenniaMatriciNaMatriciu(ryadky, H2);
                    praviSinguliarniVectori = MnojenniaMatriciNaMatriciu(Transponuvati(H2), praviSinguliarniVectori);

                    List<List<double>> H11 = new List<List<double>>();
                    p = 0;
                    for (int j = i + 1; j < m; j++)
                    {
                        p += ryadky[j][i] * ryadky[j][i];
                    }
                    p = Math.Sign(-ryadky[i + 1][i]) * Math.Sqrt(p);
                    u = 1 / (Math.Sqrt(2 * p * p - 2 * p * ryadky[i + 1][i]));
                    w = new List<double>();
                    for (int j = 0; j < m; j++)
                    {
                        if (j < i + 1)
                        {
                            w.Add(0);
                        }
                        else if (j == i + 1)
                        {
                            w.Add(u * (ryadky[j][i] - p));
                        }
                        else
                        {
                            w.Add(u * ryadky[j][i]);
                        }
                    }
                    H11 = VidnimanniaVidMatriciMatrici(OdinichnaMatricia(ryadky.Count), MnojenniaMatriciNaScaliar(2, MnojenniaVectoraNaVector(w, w)));
                    ryadky = MnojenniaMatriciNaMatriciu(H11, ryadky);
                    liviSinguliarniVectori = MnojenniaMatriciNaMatriciu(liviSinguliarniVectori, Transponuvati(H11));
                }
                

                //ryadky = VidkinutiRyadok(ryadky, ryadky.Count - 1);
            }
            for(int i = n + 1; i < m; i++)
            {
                ryadky = VidkinutiRyadok(ryadky, n + 1);
            }
            //m = ryadky.Count - 1;
            //n = ryadky[0].Count - 1;
            for (int j = n - 1; j >= 0; j--)
            {


                double t = -ryadky[n][j] / ryadky[j][j];
                List<List<double>> Givensa = OdinichnaMatricia(ryadky.Count);
                double c = Givensa[j][j] = Givensa[n][n] = 1 / Math.Sqrt(1 + t * t);
                double s = Givensa[n][j] = t / Math.Sqrt(1 + t * t);
                Givensa[j][n] = -t / Math.Sqrt(1 + t * t);
                //showMatrix(Givensa);
                //Console.WriteLine(s * s + c * c);
                ryadky = MnojenniaMatriciNaMatriciu(Givensa, ryadky);
                for (int k = Givensa.Count; k < this.m; k++)
                {
                    DokinutiRiadok(Givensa, true);
                }
                for (int k = Givensa[0].Count; k < this.m; k++)
                {
                    DokinutiStovpec(Givensa, true);
                }
                    liviSinguliarniVectori = MnojenniaMatriciNaMatriciu(liviSinguliarniVectori, Transponuvati(Givensa));
            }
            ryadky = VidkinutiRyadok(ryadky, ryadky.Count - 1);
        }
        else
        {
            for (int i = 0; i < m - 1; i++)
            {
                //if (i != n - 1)
                
                    List<List<double>> H2 = new List<List<double>>();
                    double p = 0;
                    for (int j = i; j < n; j++)
                    {
                        p += ryadky[i][j] * ryadky[i][j];
                    }

                    p = Math.Sign(-ryadky[i][i]) * Math.Sqrt(p);
                    double u = 1 / (Math.Sqrt(2 * p * p - 2 * p * ryadky[i][i]));
                    List<double> w = new List<double>();
                    for (int j = 0; j < ryadky[i].Count; j++)
                    {
                        if (j < i)
                        {
                            w.Add(0);
                        }
                        else if (j == i)
                        {
                            w.Add(u * (ryadky[j][j] - p));
                        }
                        else
                        {
                            w.Add(u * ryadky[i][j]);
                        }
                    }


                    H2 = VidnimanniaVidMatriciMatrici(OdinichnaMatricia(ryadky[0].Count), MnojenniaMatriciNaScaliar(2, MnojenniaVectoraNaVector(w, w)));
                    ryadky = MnojenniaMatriciNaMatriciu(ryadky, H2);
                praviSinguliarniVectori = MnojenniaMatriciNaMatriciu(Transponuvati(H2), praviSinguliarniVectori);
                List<List<double>> H11 = new List<List<double>>();
                    p = 0;
                    for (int j = i + 1; j < m; j++)
                    {
                        p += ryadky[j][i] * ryadky[j][i];
                    }
                    p = Math.Sign(-ryadky[i + 1][i]) * Math.Sqrt(p);
                    u = 1 / (Math.Sqrt(2 * p * p - 2 * p * ryadky[i + 1][i]));
                    w = new List<double>();
                    for (int j = 0; j < m; j++)
                    {
                        if (j < i + 1)
                        {
                            w.Add(0);
                        }
                        else if (j == i + 1)
                        {
                            w.Add(u * (ryadky[j][i] - p));
                        }
                        else
                        {
                            w.Add(u * ryadky[j][i]);
                        }
                    }
                    H11 = VidnimanniaVidMatriciMatrici(OdinichnaMatricia(ryadky.Count), MnojenniaMatriciNaScaliar(2, MnojenniaVectoraNaVector(w, w)));
                    ryadky = MnojenniaMatriciNaMatriciu(H11, ryadky);
                liviSinguliarniVectori = MnojenniaMatriciNaMatriciu(liviSinguliarniVectori, Transponuvati(H11));

                //ryadky = VidkinutiRyadok(ryadky, ryadky.Count - 1);


            }
            for (int i = ryadky.Count - 1; i < ryadky[0].Count; i++)
            {
                ryadky = VidkinutiStovpec(ryadky, ryadky.Count);
            }
    }
        //praviSinguliarniVectori = Transponuvati(praviSinguliarniVectori);
    }
    double NormaNizhniogoTrikuntnika(List<List<double>> matrix)
    {
        double norma = 0;
        for (int i = 1; i < matrix.Count; i++)
        {
            for (int j = 0; j < i; j++)
            {  if (i != j)
                {
                    norma += Math.Pow(matrix[i][j], 2);
                }
            }
        }
        return Math.Sqrt(norma);
    }
    public void DrugiyEtap()
    {
        if(ryadky.Count != 0) { 
            int r = Math.Min(ryadky.Count - 1, ryadky[0].Count - 1);
        
        List<List<double>> B = ryadky; // Transponuvati(ryadky); //MnojenniaMatriciNaMatriciu(Transponuvati(ryadky), ryadky);
        List<List<double>> Tm = OdinichnaMatricia(n);
        List<List<double>> Sm = OdinichnaMatricia(m);
        int iter = 0;
        while (NormaNizhniogoTrikuntnika(ryadky) > epsilon && ryadky.Count != 1)
        {
            /*if (NormaForbeniusa(B) < epsilon){
                break;
            }
            double f = (Math.Pow(B[n - 2][n - 1], 2) - Math.Pow(B[n - 1][n], 2) + Math.Pow(B[n - 1][n - 1], 2) - Math.Pow(B[n][n], 2)) / (2 * B[n - 1][n] * B[n - 1][n - 1]);
            double q = Math.Sqrt(1 + f * f);
            double tau = B[n][n] * B[n][n] + B[n - 1][n] * B[n - 1][n] + B[n - 1][n] * (f - q);
            double ctg = (B[0][0] * B[0][0] - tau) / (B[0][0] * B[0][1]);
            double s = 1 / (Math.Sqrt(1 + ctg * ctg));
            double c = ctg * s;
            List<List<double>> Givensa = OdinichnaMatricia(n + 1);
            Givensa[0][0] = Givensa[1][1] = c;
            Givensa[0][1] = -s;
            Givensa[1][0] = s;
            ryadky = MnojenniaMatriciNaMatriciu(ryadky, Givensa);
            double h2 = B[1][1] * s;
            double d1 = B[0][0] * c + B[0][1] * s;
            double t = h2 / d1;
            double c = 1 / Math
            showMatrix(ryadky);
            Console.ReadKey();*/
            //int r = 1;
            iter++;
            for (int i = 0; i < r; i++)
            {
                if (i != 0)
                {
                    double t = ryadky[i + 1][i - 1] / ryadky[i][i - 1];
                    double c = 1 / Math.Sqrt(1 + t * t);
                    double s = t / Math.Sqrt(1 + t * t);
                    List<List<double>> Givensa = OdinichnaMatricia(r + 1);
                    Givensa[i][i] = Givensa[i + 1][i + 1] = c;
                    Givensa[i][i + 1] = s;
                    Givensa[i + 1][i] = -s;
                    //Console.WriteLine(-s * ryadky[i][i - 1] + c * ryadky[i + 1][i - 1]);
                    double T = (ryadky[i + 1][i + 1] * s) / (ryadky[i + 1][i] * s + ryadky[i][i] * c);
                    double C = 1 / Math.Sqrt(1 + T * T);
                    double S = T / Math.Sqrt(1 + T * T);
                    ryadky = MnojenniaMatriciNaMatriciu(Givensa, ryadky);
                    for (int k = Givensa.Count; k < this.m; k++)
                    {
                        DokinutiRiadok(Givensa, true);
                    }
                    for (int k = Givensa[0].Count; k < this.m; k++)
                    {
                        DokinutiStovpec(Givensa, true);
                    }
                    Sm = MnojenniaMatriciNaMatriciu(Sm, Transponuvati(Givensa));
                    
                    Givensa = OdinichnaMatricia(r + 1);
                    Givensa[i][i] = Givensa[i + 1][i + 1] = C;
                    Givensa[i][i + 1] = -S;
                    Givensa[i + 1][i] = S;
                    ryadky = MnojenniaMatriciNaMatriciu(ryadky, Givensa);
                    for (int k = Givensa.Count; k < this.n; k++)
                    {
                        DokinutiRiadok(Givensa, true);
                    }
                    for (int k = Givensa[0].Count; k < this.n; k++)
                    {
                        DokinutiStovpec(Givensa, true);
                    }
                    Tm = MnojenniaMatriciNaMatriciu(Transponuvati(Givensa), Tm);
                    
                }
                else
                {
                        /*n = n - 1;
                        double f = (B[n][n] * B[n][n] - B[n - 1][n - 1] * B[n - 1][n - 1] - B[n][n - 2] * B[n][n - 2]) / 2 * B[n][n] * B[n][n - 1]; //(Math.Pow(B[n - 2][n - 1], 2) - Math.Pow(B[n - 1][n], 2) + Math.Pow(B[n - 1][n - 1], 2) - Math.Pow(B[n][n], 2)) / (2 * B[n - 1][n] * B[n - 1][n - 1]);
                        double re = 0;
                        if(f >= 0){
                            re = -f - Math.Sqrt(1 + f * f);
                        }
                        else
                        {
                            re = -f + Math.Sqrt(1 + f * f);
                        }

                        double tau = B[n][n] * B[n][n] - B[n][n] * B[n][n - 1] / re; //B[n][n] * B[n][n] + B[n - 1][n] * B[n - 1][n] + B[n - 1][n] * B[n][n] * (f - q);
                        double ctg = (B[1][1] * B[1][1] +B[0][1] * B[0][1] - tau) / (B[0][0] * B[0][1]);
                        double s = 1 / (Math.Sqrt(1 + ctg * ctg));
                        double c = ctg * s;
                        List<List<double>> Givensa = OdinichnaMatricia(n + 1);
                        Givensa[0][0] = Givensa[1][1] = c;
                        Givensa[0][1] = s;
                        Givensa[1][0] = -s;
                        ryadky = MnojenniaMatriciNaMatriciu(Givensa, ryadky);
                        double h2 = B[1][1] * s;
                        double d1 = B[0][0] * c + B[0][1] * s;
                        double t = (s * B[1][1]) / );
                         s = t / Math.Sqrt(1 + t *(c * B[0][0] + B[0][1] * s);//
                        c = 1 / Math.Sqrt(1 + t * t t);
                        Givensa = OdinichnaMatricia(n + 1);
                        Givensa[0][0] = Givensa[1][1] = c;
                        Givensa[0][1] = -s;
                        Givensa[1][0] = s;
                        ryadky = MnojenniaMatriciNaMatriciu(ryadky, Givensa);
                        n++;*/
                        int o = ryadky.Count - 1;
                        double f = (B[o][o] * B[o][o] - B[o - 1][o - 1] * B[o - 1][o - 1] - B[o][o - 2] * B[o][o - 2]) / 2 * B[o][o] * B[o][o - 1]; //(Math.Pow(B[n - 2][n - 1], 2) - Math.Pow(B[n - 1][n], 2) + Math.Pow(B[n - 1][n - 1], 2) - Math.Pow(B[n][n], 2)) / (2 * B[n - 1][n] * B[n - 1][n - 1]);
                        double re = 0;
                        if (f >= 0)
                        {
                            re = -f - Math.Sqrt(1 + f * f);
                        }
                        else
                        {
                            re = -f + Math.Sqrt(1 + f * f);
                        }

                        double tau = B[o][o] * B[o][o] - B[o][o] * B[o][o - 1] / re; //B[n][n] * B[n][n] + B[n - 1][n] * B[n - 1][n] + B[n - 1][n] * B[n][n] * (f - q);
                        double t = (B[0][0] * B[1][0] - tau) / (B[1][1] * B[1][1] + B[1][0] * B[1][0]); 
                        double c = 1 / Math.Sqrt(1 + t * t);//0.05;
                        double s = t / Math.Sqrt(1 + t * t); //Math.Sqrt(1 - c * c);
                    List<List<double>> Givensa = OdinichnaMatricia(r + 1);
                    Givensa[0][0] = Givensa[1][1] = c;
                    Givensa[0][1] = s;
                    Givensa[1][0] = -s;
                    double T = (ryadky[1][1] * s) / (ryadky[1][0] * s + ryadky[0][0] * c);
                    double C = 1 / Math.Sqrt(1 + T * T);
                    double S = T / Math.Sqrt(1 + T * T);
                    ryadky = MnojenniaMatriciNaMatriciu(Givensa, ryadky);
                    for (int k = Givensa.Count; k < this.m; k++)
                    {
                        DokinutiRiadok(Givensa, true);
                    }
                    for (int k = Givensa[0].Count; k < this.m; k++)
                    {
                        DokinutiStovpec(Givensa, true);
                    }
                    Sm = MnojenniaMatriciNaMatriciu(Sm, Transponuvati(Givensa));
                    
                    Givensa = OdinichnaMatricia(r + 1);
                    Givensa[0][0] = Givensa[1][1] = C;
                    Givensa[0][1] = -S;
                    Givensa[1][0] = S;
                    ryadky = MnojenniaMatriciNaMatriciu(ryadky, Givensa);
                    
                    for (int k = Givensa.Count; k < this.n; k++)
                    {
                        DokinutiRiadok(Givensa, true);
                    }
                    for (int k = Givensa[0].Count; k < this.n; k++)
                    {
                        DokinutiStovpec(Givensa, true);
                    }
                    Tm = MnojenniaMatriciNaMatriciu(Transponuvati(Givensa), Tm); 
                    /*List<List<double>> Givensa = OdinichnaMatricia(r + 1);
                    n = n - 1;
                    double f = (B[n][n] * B[n][n] - B[n - 1][n - 1] * B[n - 1][n - 1] - B[n][n - 1] * B[n][n - 1]) / 2 * B[n][n] * B[n][n - 1]; //(Math.Pow(B[n - 2][n - 1], 2) - Math.Pow(B[n - 1][n], 2) + Math.Pow(B[n - 1][n - 1], 2) - Math.Pow(B[n][n], 2)) / (2 * B[n - 1][n] * B[n - 1][n - 1]);
                    double re = 0;
                    if (f >= 0)
                    {
                        re = -f - Math.Sqrt(1 + f * f);
                    }
                    else
                    {
                        re = -f + Math.Sqrt(1 + f * f);
                    }

                    double tau = B[n][n] * B[n][n] - B[n][n] * B[n][n - 1] / re; //B[n][n] * B[n][n] + B[n - 1][n] * B[n - 1][n] + B[n - 1][n] * B[n][n] * (f - q);
                    double ctg = (B[1][1] * B[1][1] + B[1][0] * B[1][0] - tau) / (B[0][0] * B[1][0]);
                    double s = 1 / (Math.Sqrt(1 + ctg * ctg));
                    double c = ctg * s;
                    Givensa[0][0] = Givensa[1][1] = c;
                    Givensa[0][1] = -s;
                    Givensa[1][0] = s;
                    double T = (ryadky[0][0] * s) / (ryadky[1][0] * s + ryadky[2][2] * c);
                    double C = 1 / Math.Sqrt(1 + T * T);
                    double S = T / Math.Sqrt(1 + T * T);
                    ryadky = MnojenniaMatriciNaMatriciu(ryadky, Givensa);
                    for (int k = Givensa.Count; k < this.n; k++)
                    {
                        DokinutiRiadok(Givensa, true);
                    }
                    for (int k = Givensa[0].Count; k < this.n; k++)
                    {
                        DokinutiStovpec(Givensa, true);
                    }
                    Tm = MnojenniaMatriciNaMatriciu(Transponuvati(Givensa), Tm);

                    Givensa = OdinichnaMatricia(r + 1);
                    Givensa[0][0] = Givensa[1][1] = C;
                    Givensa[0][1] = -S;
                    Givensa[1][0] = S;
                    ryadky = MnojenniaMatriciNaMatriciu(Givensa, ryadky);
                    for (int k = Givensa.Count; k < this.m; k++)
                    {
                        DokinutiRiadok(Givensa, true);
                    }
                    for (int k = Givensa[0].Count; k < this.m; k++)
                    {
                        DokinutiStovpec(Givensa, true);
                    }
                    Sm = MnojenniaMatriciNaMatriciu(Sm, Transponuvati(Givensa));
                    n++;*/
                    
                    
                    
                }

            }
            if (NormaNizhniogoTrikuntnika(ryadky) <= epsilon)
            {
                ready = true;
                //Console.WriteLine(Math.Sqrt(NormaNizhniogoTrikuntnika(ryadky)));
                break;
            }
                
        }
        liviSinguliarniVectori = MnojenniaMatriciNaMatriciu(liviSinguliarniVectori, Sm);
        praviSinguliarniVectori = MnojenniaMatriciNaMatriciu(Tm, praviSinguliarniVectori);
        Console.WriteLine("Ітерацій: " + iter);
        }
        else
        {
            ready = true;
        }

    }
    
}
class Matrix1 : Matrix
{
    public Matrix1(): base(7, 5)
    {
        ryadky[0][0] = -145;
        ryadky[0][1] = 39;
        ryadky[0][2] = -12;
        ryadky[0][3] = 127;
        ryadky[0][4] = 86;

        ryadky[1][0] = -63;
        ryadky[1][1] = 107;
        ryadky[1][2] = -41;
        ryadky[1][3] = 3;
        ryadky[1][4] = 31;

        ryadky[2][0] = 143;
        ryadky[2][1] = 120;
        ryadky[2][2] = -66;
        ryadky[2][3] = 125;
        ryadky[2][4] = -10;

        ryadky[3][0] = 80;
        ryadky[3][1] = -46;
        ryadky[3][2] = -128;
        ryadky[3][3] = -123;
        ryadky[3][4] = -61;

        ryadky[4][0] = -77;
        ryadky[4][1] = -4;
        ryadky[4][2] = -17;
        ryadky[4][3] = 148;
        ryadky[4][4] = -110;

        ryadky[5][0] = 55;
        ryadky[5][1] = 54;
        ryadky[5][2] = -101;
        ryadky[5][3] = -121;
        ryadky[5][4] = -62;

        ryadky[6][0] = -109;
        ryadky[6][1] = 61;
        ryadky[6][2] = -30;
        ryadky[6][3] = -56;
        ryadky[6][4] = -100;

        normaNeskinchennist = NormaNeskinchennist(ryadky);
        b = new List<double>() { -55, -117, 100, 142, -85, 62, -139 };
    }
}

class Matrix2: Matrix
{
    public Matrix2() : base(5, 7)
    {
        ryadky[0][0] = -25;
        ryadky[0][1] = -53;
        ryadky[0][2] = 63;
        ryadky[0][3] = -11;
        ryadky[0][4] = 120;
        ryadky[0][5] = 110;
        ryadky[0][6] = -121;

        ryadky[1][0] = -66;
        ryadky[1][1] = -121;
        ryadky[1][2] = 43;
        ryadky[1][3] = -89;
        ryadky[1][4] = -14;
        ryadky[1][5] = 35;
        ryadky[1][6] = 21;

        ryadky[2][0] = 30;
        ryadky[2][1] = -99;
        ryadky[2][2] = -98;
        ryadky[2][3] = 111;
        ryadky[2][4] = -133;
        ryadky[2][5] = -142;
        ryadky[2][6] = -52;

        ryadky[3][0] = -140;
        ryadky[3][1] = -39;
        ryadky[3][2] = -132;
        ryadky[3][3] = 29;
        ryadky[3][4] = -119;
        ryadky[3][5] = -53;
        ryadky[3][6] = -15;

        ryadky[4][0] = -131;
        ryadky[4][1] = -139;
        ryadky[4][2] = -28;
        ryadky[4][3] = -144;
        ryadky[4][4] = 150;
        ryadky[4][5] = -11;
        ryadky[4][6] = 23;
        normaNeskinchennist = NormaNeskinchennist(ryadky);
        b = new List<double>()
        { 136, -150, -62, -136, -17
        };
    }
}
namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix1 m1 = new Matrix1();
            m1.SinguliarniyRozklad();
            /*Matrix.showMatrix(Matrix.Transponuvati(m1.ryadky));
            Matrix.showMatrix(Matrix.MnojenniaMatriciNaMatriciu(m1.ryadky, Matrix.Transponuvati(m1.ryadky)));
            //Console.WriteLine("Hello World!");*/
            //m1.PershiyEtap();
            //Console.WriteLine();
            //m1.DrugiyEtap();
            //Matrix.showMatrix(m1.ryadky);
            Console.ReadKey();
            Console.Clear();
            Matrix2 m2 = new Matrix2();
            m2.SinguliarniyRozklad();
            //m2.PershiyEtap();
            //m2.DrugiyEtap();
            //Matrix.showMatrix(m2.ryadky);
        }
    }
}
